#include <SPI.h>
#include <MFRC522.h>
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

#define PIN_RESET 9 // SPI Reset Pin
#define PIN_SS 10 // SPI Slave Select Pin

LiquidCrystal_I2C lcd(0x27, 16, 2); // Load LCD
MFRC522 mfrc522(PIN_SS, PIN_RESET); // Load RFID-Scanner

String alcohol = "Vodka"; // Define the alcoholic drink
String drinks[] = {"Cola", "Orangensaft"}; // Define the 2 mixing drinks

// Arduino Pins
int led_red_pin = 3;
int led_green_pin = 5;
int led_blue_pin = 6;
int potentio_pin = A0;
int touch_sensor_pin = 2;
int drink_1_pump_pin = 4;
int drink_2_pump_pin = 7;
int alcohol_pump_pin = 8;

// Values for final step
int selected_cl = 0;
int selected_drink = 0;
int selected_percent = 10;
int ml_alcohol = 0;
int ml_drink = 0;

// Value for Admin GUI
int selected_admin_pump = 0;

// State values
int current_state = 1;
int touch_state = 0;
int confirm_state = 0;

void setup() {
  // Initialize LCD
  lcd.init();
  lcd.backlight();

  // Initialize RFID-Scanner
  SPI.begin();
  mfrc522.PCD_Init();

  // Initilize Pins
  pinMode(led_red_pin, OUTPUT);
  pinMode(led_green_pin, OUTPUT);
  pinMode(led_blue_pin, OUTPUT);
  pinMode(potentio_pin, INPUT);
  pinMode(touch_sensor_pin, INPUT);
  pinMode(drink_1_pump_pin, OUTPUT);
  pinMode(drink_2_pump_pin, OUTPUT);
  pinMode(alcohol_pump_pin, OUTPUT);

  // Start pumps until hoses are full
  delay(1000);
  digitalWrite(drink_1_pump_pin, HIGH);
  digitalWrite(drink_2_pump_pin, HIGH);
  digitalWrite(alcohol_pump_pin, HIGH);
  delay(2000);
  digitalWrite(drink_1_pump_pin, LOW);
  digitalWrite(drink_2_pump_pin, LOW);
  digitalWrite(alcohol_pump_pin, LOW);
}

void loop() {
  int potentio_value = analogRead(potentio_pin); // Read potentiometer

  switch (current_state) {
    case 0:
      RGB_color(0, 0, 255); // blue
      show_admin_GUI(potentio_value);
      break;
    case 1:
      RGB_color(0, 255, 0); // green
      show_select_volume_GUI(potentio_value);
      break;
    case 2:
      RGB_color(255, 140, 0); // orange
      show_select_drink_GUI(potentio_value);
      break;
    case 3:
      RGB_color(255, 140, 0); // orange
      show_select_percentage_GUI(potentio_value);
      break;
    case 4:
      RGB_color(255, 140, 0); // orange
      show_info_GUI();
      current_state = 5;
      break;
    case 5:
      RGB_color(255, 140, 0); // orange
      show_confirm_GUI(potentio_value);
      break;
    case 6:
      RGB_color(255, 0, 0); // red
      show_running_GUI();
      current_state = 1;
      break;
    default:
      current_state = 1;
      break;
  }

  int touch_value = digitalRead(touch_sensor_pin); // Read Touchsensor
  if (current_state == 0) { // If adminstate
    if (touch_value == 1) {
      if (selected_admin_pump == 0) {
        digitalWrite(drink_1_pump_pin, HIGH);
      } else if (selected_admin_pump == 1) {
        digitalWrite(drink_2_pump_pin, HIGH);
      } else if (selected_admin_pump == 2) {
        digitalWrite(alcohol_pump_pin, HIGH);
      }
    } else {
      digitalWrite(drink_1_pump_pin, LOW);
      digitalWrite(drink_2_pump_pin, LOW);
      digitalWrite(alcohol_pump_pin, LOW);
    }
  } else {
    if (touch_value == 1 && touch_state == 0) {
      touch_state = 1;
      current_state++;
      lcd.clear();
    } else if (touch_value == 0 && touch_state == 1) {
      touch_state = 0;
    }
  }

  // Read RFID
  if (mfrc522.PICC_IsNewCardPresent() && mfrc522.PICC_ReadCardSerial()) {
    String uid = "";
    for (byte i = 0; i < mfrc522.uid.size; i++) {
      uid += mfrc522.uid.uidByte[i] < 0x10 ? " 0x0" : " 0x";
      uid += mfrc522.uid.uidByte[i], HEX;
    }
    mfrc522.PICC_HaltA();
    // If rfid uid is correct
    if (uid == " 0x134 0x141 0x163 0x37") {
      // If user is in adminstate, leave admin state. Else enter adminstate
      if (current_state == 0) {
        RGB_color(0, 255, 0); // green
        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print("Auf");
        lcd.setCursor(0, 1);
        lcd.print("Wiedersehen");
        delay(2000);
        lcd.clear(); 
        current_state = 1;
      } else {
        RGB_color(0, 255, 0); // green
        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print("Willkommen");
        lcd.setCursor(0, 1);
        lcd.print("Zum AdminGUI");
        delay(2000);
        lcd.clear();
        current_state = 0;
      }
    } else { // No permissions
      RGB_color(255, 0, 0); // red
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Keine");
      lcd.setCursor(0, 1);
      lcd.print("Berechtigung");
      delay(2000);
      lcd.clear();
      current_state = 1;
    }
  }
}

void show_select_volume_GUI(int potentio_value) {
  int cl = map(potentio_value, 0, 1023, 10, 50);
  lcd.setCursor(0, 0);
  lcd.print("Wie viel CL?");
  lcd.setCursor(0, 1);
  lcd.print(cl);
  lcd.print("cl");
  selected_cl = cl;
}

void show_select_drink_GUI(int potentio_value) {
  int selectedGetraenk = potentio_value < 512 ? 0 : 1;
  if (selected_drink != selectedGetraenk) {
    selected_drink = selectedGetraenk;
    lcd.clear();
  }
  lcd.setCursor(0, 0);
  lcd.print("Getr");
  lcd.print((char) 225);
  lcd.print("nk?");
  lcd.setCursor(0, 1);
  lcd.print(drinks[selectedGetraenk]);
}

void show_select_percentage_GUI(int potentio_value) {
  int percent = map(potentio_value, 0, 1023, 10, 90);
  lcd.setCursor(0, 0);
  lcd.print("Prozent ");
  lcd.print(alcohol);
  lcd.print("?");
  lcd.setCursor(0, 1);
  lcd.print(percent);
  lcd.print((char) 37);
  selected_percent = percent;
}

void show_info_GUI() {
  int ml = selected_cl * 10;
  ml_alcohol = round(ml / 100 * selected_percent);
  ml_drink = round(ml - ml_alcohol);
  lcd.setCursor(0, 0);
  lcd.print(ml_alcohol);
  lcd.print("ml ");
  lcd.print(alcohol);
  lcd.setCursor(0, 1);
  lcd.print(ml_drink);
  lcd.print("ml ");
  lcd.print(drinks[selected_drink]);
  delay(3000);
  lcd.clear();
}

void show_confirm_GUI(int potentio_value) {
  int confirm = potentio_value < 512 ? false : true;
  if (confirm != confirm_state) {
    confirm_state = confirm;
    lcd.clear();
  }
  lcd.setCursor(0, 0);
  lcd.print("Best");
  lcd.print((char) 225);
  lcd.print("tigen?");
  lcd.setCursor(0, 1);
  if (confirm) {
    lcd.print("Ja");
  } else {
    lcd.print("Nein");
  }
}

void show_running_GUI() {
  if (!confirm_state) {
    current_state = 1;
    return;
  }
  lcd.setCursor(0, 0);
  lcd.print("Glas nicht");
  lcd.setCursor(0, 1);
  lcd.print("Entfernen");
  digitalWrite(8, HIGH);
  delay(ml_alcohol * 33);
  digitalWrite(8, LOW);
  if (selected_drink == 0) {
    digitalWrite(4, HIGH);
  } else if (selected_drink == 1) {
    digitalWrite(7, HIGH);
  }
  delay(ml_drink * 33);
  digitalWrite(4, LOW);
  digitalWrite(7, LOW);
  lcd.clear();
  reset_values();
}

void show_admin_GUI(int potentio_value) {
  int drink = potentio_value < 340 ? 0 : 1;
  if (drink == 1) {
   drink = potentio_value < 680 ? 1 : 2;
  }
  String all_drinks[] = {drinks[0], drinks[1], alcohol};
  if (selected_admin_pump != drink) {
    selected_admin_pump = drink;
    lcd.clear();
  }
  lcd.setCursor(0, 0);
  lcd.print("Getr");
  lcd.print((char) 225);
  lcd.print("nk Pumpe:");
  lcd.setCursor(0, 1);
  lcd.print(all_drinks[drink]);
}

void RGB_color(int red_light_value, int green_light_value, int blue_light_value) {
  analogWrite(3, red_light_value);
  analogWrite(5, green_light_value);
  analogWrite(6, blue_light_value);
}

void reset_values() {
  selected_cl = 0;
  selected_drink = 0;
  selected_percent = 10;
  ml_alcohol = 0;
  ml_drink = 0;
  selected_admin_pump = 0;
  current_state = 1;
  touch_state = 0;
  confirm_state = 0;
}
