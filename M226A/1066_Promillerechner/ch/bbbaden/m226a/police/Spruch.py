class Spruch:
    def __init__(self, alkohol_promille):
        self.__alkohol_promille = alkohol_promille

    def get_spruch(self):
        if self.__alkohol_promille < 0.5:
            return "Autofahre lauft schono\n" + str(self.__alkohol_promille)
        elif self.__alkohol_promille < 1.0:
            return "Gang mol go witer suffe\n" + str(self.__alkohol_promille)
        else:
            return "Lüg ned\n" + str(self.__alkohol_promille)