from ..Person import Person
from ..AlkoholischesGetraenk import AlkoholischesGetraenk
from ..Spruch import Spruch
from datetime import date, timedelta, datetime
import unittest


class TestSpruch(unittest.TestCase):
    def test_tief(self):
        person = Person(88, 176, date(date.today().year - 45, date.today().month, date.today().day), 1)
        getraenk = AlkoholischesGetraenk(500, AlkoholischesGetraenk.BIER_ALKOHOLGEHALT,
                                         datetime.now() - timedelta(minutes=0.5))
        person.trinke(getraenk)
        spruch = Spruch(person.get_alkohol_promille())

        self.assertEqual(spruch.get_spruch(), "Autofahre lauft schono\n" + str(person.get_alkohol_promille()))

    def test_mittel(self):
        person = Person(88, 176, date(date.today().year - 45, date.today().month, date.today().day), 1)
        getraenk = AlkoholischesGetraenk(1200, AlkoholischesGetraenk.BIER_ALKOHOLGEHALT,
                                         datetime.now() - timedelta(minutes=0.5))
        person.trinke(getraenk)
        spruch = Spruch(person.get_alkohol_promille())

        self.assertEqual(spruch.get_spruch(), "Gang mol go witer suffe\n" + str(person.get_alkohol_promille()))

    def test_hart(self):
        person = Person(88, 176, date(date.today().year - 45, date.today().month, date.today().day), 1)
        getraenk = AlkoholischesGetraenk(2000, AlkoholischesGetraenk.BIER_ALKOHOLGEHALT,
                                         datetime.now() - timedelta(minutes=0.5))
        person.trinke(getraenk)
        spruch = Spruch(person.get_alkohol_promille())

        self.assertEqual(spruch.get_spruch(), "Lüg ned\n" + str(person.get_alkohol_promille()))
