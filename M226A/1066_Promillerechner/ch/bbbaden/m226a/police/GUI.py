from datetime import datetime
from Person import *
from AlkoholischesGetraenk import *
from tools.Inputbox import Inputbox
from Spruch import Spruch


class GUI:
    def __ask_person_data(self):
        try:
            koerpermasse = float(Inputbox("Geben Sie Ihr Gewicht in kg ein.").get)
        except ValueError:
            print('Bitte eine Kommazahl eingeben')
            exit()

        try:
            koerpergroesse_in_cm = float(Inputbox("Geben Sie Ihre Grösse in cm ein.").get)
        except ValueError:
            print('Bitte eine Kommazahl eingeben')
            exit()
        try:
            geburtsdatum = datetime.strptime(Inputbox("Geben Sie Ihr Geburtsdatum ein.(dd.mm.yyyy)").get, '%d.%m.%Y')
        except:
            print('Bitte ein Datum eingeben')
            exit()

        geschlecht_angabe = Inputbox("Geben Sie Ihr Geschlecht an. (m/w)").get
        if geschlecht_angabe == 'm':
            geschlecht = Person.MAENNLICH
        elif geschlecht_angabe == 'w':
            geschlecht = Person.MAENNLICH
        else:
            print('Bitte m oder w eingeben')
            exit()
        
        return Person(koerpermasse, koerpergroesse_in_cm, geburtsdatum, geschlecht)

    def __ask_alkoholische_getraenke_data(self, trink_datum):
        try:
            volumen_in_milli_liter = int(Inputbox("Geben Sie die getrunkene Menge in ml ein.").get)
        except ValueError:
            print('Bitte eine Zahl eingeben')
            exit()

        getraenk_angabe = Inputbox("Was für ein Getränk haben Sie getrunken?(Bier/Wein/Schnaps)").get
        if getraenk_angabe == 'Bier':
            alkoholgehalt = AlkoholischesGetraenk.BIER_ALKOHOLGEHALT
        elif getraenk_angabe == 'Wein':
            alkoholgehalt = AlkoholischesGetraenk.WEIN_ALKOHOLGEHALT
        elif getraenk_angabe == 'Schnaps':
            alkoholgehalt = AlkoholischesGetraenk.SCHNAPS_ALKOHOLGEHALT
        else:
            print('Bitte Bier, Wein oder Schnaps eingeben')
            exit()


        return AlkoholischesGetraenk(volumen_in_milli_liter, alkoholgehalt, trink_datum)

    def promille_rechner(self):
        person = self.__ask_person_data()
        try:
            trink_datum = datetime.strptime(Inputbox("Geben Sie Trinkdatum und Zeit ein.(dd.mm.yyyy hh:mm)").get, '%d.%m.%Y %H:%M')
        except:
            print('Bitte ein Datum eingeben')
            exit()
        getraenk = self.__ask_alkoholische_getraenke_data(trink_datum)
        person.trinke(getraenk)
        spruch = Spruch(person.get_alkohol_promille())
        Inputbox(spruch.get_spruch())