from datetime import date

class Person:
    MAENNLICH = 0
    WEIBLICH = 1
    __ABBAU_WARTEZEIT_STUNDEN = 1.0
    __ABBAU_PRO_STUNDE = 0.1
    __ANTEIL_WASSER_IM_BLUT = 0.8
    __DICHTE_BLUT_GRAMM_PRO_CCM = 1.055

    __alkhol_promille = 0.0

    def __init__(self, koerpermasse, koerpergroesse_in_cm, geburtsdatum, geschlecht):
        self.__koerpermasse = koerpermasse
        self.__koerpergroesse_in_cm = koerpergroesse_in_cm
        self.__geburtsdatum = geburtsdatum
        self.__geschlecht = geschlecht

    def __get_alter_in_jahren(self):
        heute = date.today() 
        age = heute.year - self.__geburtsdatum.year - ((heute.month, heute.day) < (self.__geburtsdatum.month, self.__geburtsdatum.day)) 
        return age

    def trinke(self, alkoholisches_getraenk):
        self.__alkhol_promille = (self.__ANTEIL_WASSER_IM_BLUT * alkoholisches_getraenk.get_alkohol_masse_in_gramm()) / (self.__DICHTE_BLUT_GRAMM_PRO_CCM * self.getGKW())

    def get_alkohol_promille(self):
        return self.__alkhol_promille

    def getGKW(self):
        gkw = 0
        if self.__geschlecht == self.MAENNLICH:
            gkw = 2.447 - 0.09516 * self.__get_alter_in_jahren() + 0.1074 * self.__koerpergroesse_in_cm + 0.3362 * self.__koerpermasse
        elif self.__geschlecht == self.WEIBLICH:
            gkw = 0.203 - 0.07 * self.__get_alter_in_jahren() + 0.1069 * self.__koerpergroesse_in_cm + 0.2466 * self.__koerpermasse
        return gkw
