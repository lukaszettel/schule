from datetime import datetime
class AlkoholischesGetraenk:
    BIER_ALKOHOLGEHALT = 0.05
    WEIN_ALKOHOLGEHALT = 0.10
    SCHNAPS_ALKOHOLGEHALT = 0.40
    __DICHTE_ALKOHOL = 0.8

    def __init__(self, volumen_in_milli_liter, alkoholgehalt, getrunken_am):
        self.__volumen_in_milli_liter = volumen_in_milli_liter
        self.__alkoholgehalt = alkoholgehalt
        self.__getrunken_am = getrunken_am

    def get_stunden_seit_einnahme(self, jetzt):
        differenz = datetime.now() - jetzt
        return differenz.total_seconds() / 3600
    
    def get_alkohol_masse_in_gramm(self):
        return self.__volumen_in_milli_liter * self.__alkoholgehalt * self.__DICHTE_ALKOHOL