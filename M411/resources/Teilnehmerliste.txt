Zehnder;Fabian;87;WXdenswil;2:48:15;M18
Blaettler;Fredy;78;Hergiswil;2:37:16;M20
Engeli;Benjamin;79;Wiesendangen;2:54:07;M20
Geiser;Dominique;79;Binningen;2:54:45;M20
HaXlinger;Johannes;78;D-Nienburg;2:43:12;M20
Havlicek;Jan;79;CZ-Prag;2:42:05;M20
Hombo;Lucian Disdery;82;TAN-Tanzania;2:18:13;M20
Irunde;Daudi;79;TAN-Tanzania;2:21:11;M20
Khoklov;Alexei;77;RUS-Russland;2:18:54;M20
Kosgei;David;77;KEN-Kenya;2:21:28;M20
Kraus;Benjamin;78;D-Heroldsbach;2:43:22;M20
Maier;Matthias;79;ZXrich;2:52:19;M20
Mengesha;Feyisa;78;ETH-Uznach;2:29:13;M20
Muia;Phillip;82;KEN-Kenya;2:21:00;M20
Mwiti;Douglas;82;KEN-Kenya;2:28:58;M20
Ott;Michael;82;Kilchberg;2:29:50;M20
Peindl;Hermann;80;A-Ilz;2:42:31;M20
Reichenbach;Hans;79;Latterbach;2:35:02;M20
Rupp;Patrick;78;Oberembrach;2:48:30;M20
Sass;Alexander;78;Genf;2:57:18;M20
Schmid;Stefan;81;D-TXbingen;2:38:31;M20
Schnider;Roman;78;D-Karlsruhe;2:37:29;M20
Stillman;David;81;Ebertswil;2:33:44;M20
Studer;Martin;78;Kloten;2:52:14;M20
Sumaye;Paul;83;TAN-Tanzania;2:23:58;M20
Veselov;Alexei;79;RUS-Moskau;2:21:07;M20
VonXsch;Matthias;82;Vordemwald;2:55:42;M20
Wyss;Rafael;82;Dulliken;2:39:41;M20
Yatich;Kibor Alfonse;83;KEN-Kenya;2:12:54;M20
Zehnder;Roland;79;Altendorf;2:40:53;M20
Achleitner;Gregor;71;Baar;2:53:59;M30
AnXay;Emmanuel;72;Martigny;2:50:22;M30
Atkins;Benjamin;73;Basel;2:40:50;M30
Aymon;Jean-Claude;68;Ayent;2:38:54;M30
BXr;This;67;ZXrich;2:56:41;M30
Battaglia;Simon;69;Schlieren;2:52:59;M30
Belser;Martin;70;BXtschwil;2:37:45;M30
Berger;Beat;75;Winterthur;2:39:09;M30
Biotti;Andreas;69;Luzern;2:53:31;M30
Bischofberger;Guido;68;Oberegg;2:54:18;M30
Blattmann;Heiner;75;Winterthur;2:37:37;M30
Bogush;Dumitru;71;Schlieren;2:53:45;M30
Breitenmoser;Stefan;67;Appenzell;2:55:56;M30
BrXgger;Markus;68;Interlaken;2:45:15;M30
Brunner;Andreas;75;Stans;2:57:13;M30
Bucher;Mark;74;Luzern;2:54:18;M30
Buddeenberg;Thomas;70;D-Dortmund;2:50:06;M30
Burgermeister;Urs;70;ZXrich;2:41:48;M30
Burkhard;Beat;75;ZXrich;2:50:28;M30
Christen;Martin;70;SchXtz;2:35:31;M30
DXhler;Beat;70;Muhen;2:43:25;M30
Dambach;RenX;69;Dielsdorf;2:44:50;M30
Dupasquier;Patrice;67;Avry-devant-Pont;2:38:17;M30
Eisenring;RenX;72;Oberuzwil;2:53:00;M30
Ferrat;Pascal;73;Moosseedorf;2:54:20;M30
Fischer;Kai;69;Dietikon;2:36:55;M30
Forster;Peter;73;ZXrich;2:34:21;M30
Frei;Rolf;70;BXretswil;2:37:37;M30
Frieden;Thomas;69;Rikon im TXsstal;2:41:58;M30
Gehrig;Roman;69;ZXrich;2:49:47;M30
Gerardis;Michail;73;GR-Attiki;2:37:55;M30
Gerber;Roger;70;Biberist;2:40:18;M30
Germanier;StXphane;75;Sion;2:49:24;M30
Giannelis;Dimitrios;75;GR-Athens;2:47:33;M30
Gobeli;Kurt;73;Lenk im Simmental;2:52:16;M30
Greier;Elmar;68;A-Neustift;2:51:38;M30
Haenni;Mario;72;St. Ursen;2:50:39;M30
Holenweg;Markus;72;Attiswil;2:50:00;M30
HXrner;Jakob;73;D-Stadtbergen;2:37:47;M30
Jeanneret;Yvain;74;Le Locle;2:39:28;M30
Jenny;Beat;71;Hirzel;2:50:10;M30
Jermini;Davide;68;Cagiallo;2:57:07;M30
Jobin;Michel;68;Birmenstorf AG;2:45:10;M30
Joller;Simon;69;Aarau;2:56:45;M30
Kakui;Francis;76;KEN-Kenya;2:20:48;M30
Kern;Roger;73;Lindau;2:52:15;M30
Kindle;Hermann;70;FL-Triesen;2:49:50;M30
Kiprono;Isaac;73;KEN-Kenya;2:37:56;M30
Klingenfuss;Reto;73;ZXrich;2:56:21;M30
Klomp;Gregor;72;Gockhausen;2:48:02;M30
Kobel;Ueli;70;Affoltern i.E.;2:50:16;M30
Koller;Pius;71;Zofingen;2:54:55;M30
Kulik;Karsten;72;ZXrich;2:56:24;M30
Kuppelwieser;Florian;67;I-Latsch;2:50:25;M30
Lang;Erwin;67;EmmenbrXcke;2:57:00;M30
Lauber;Pascal;71;Morlon;2:51:06;M30
Lomas;Bryan;69;GB-Cheshire CW11 3FX;2:45:40;M30
Longchamp;Philippe;74;GenXve;2:42:46;M30
Loser;Roman;76;Richterswil;2:41:37;M30
LXthold;Edi;67;Alpnach-Dorf;2:40:33;M30
MXchler;Emil;69;Lachen SZ;2:56:24;M30
Marti;Fredi;74;AltstXtten;2:29:37;M30
Mccombie;Gregor;76;ZXrich;2:51:09;M30
Mcgarva;Adrian;74;ZXrich;2:44:38;M30
Mencattini;Yvan;69;Puplinge;2:56:27;M30
Meyer;Roland;74;Zollikofen;2:51:17;M30
Meyer;Thomas;71;Neftenbach;2:51:36;M30
Monette;Simon-Pierre;76;Baden;2:50:25;M30
Monteruccioli;Flavio;72;I-Imola;2:42:46;M30
Moos;Yvan;73;Ayent;2:37:17;M30
Morath;Pierre;70;Carouge GE;2:36:47;M30
Mutisya;Peter;74;KEN-Kenya;2:22:36;M30
Oberli;Thomas;70;RXmlang;2:52:12;M30
Pawlik;Mario;75;D-Bruchsal;2:51:37;M30
Payot;Anthony;67;F-Vetraz-Monthoux;2:51:38;M30
Puls;Klaas;73;Zofingen;2:46:28;M30
Reinauer;Klaus;69;D-Istein;2:51:28;M30
Reissinger;Kai;71;D-Pleinfeld;2:44:43;M30
Rist;Philip;71;Jona;2:25:30;M30
Rotich;John Kipngeno;69;KEN-Kenya;2:18:44;M30
RXedi;Patrick;73;ZXrich;2:40:31;M30
Ruf;Remo;74;Flims Waldhaus;2:39:51;M30
RXttimann;Marco;71;Goldach;2:52:52;M30
Scheiwiller;Guy;67;Schindellegi;2:45:20;M30
Scherrer;Michael;72;Schmerikon;2:55:57;M30
Schmid;Philipp;72;Schaffhausen;2:44:25;M30
Schneble;Gerhard;69;D-Gailingen;2:28:10;M30
Schnorf;Oliver;70;Zuerich;2:56:31;M30
Schramm;CXdric;76;F-Sarreguemines;2:56:11;M30
Schuler;Brido;73;WXdenswil;2:56:00;M30
Schwarz;Jwan;72;Elsau - RXterschen;2:34:52;M30
Schwendimann;Stefan;68;Neftenbach;2:45:33;M30
Siegert;Roland;73;KronbXhl;2:52:35;M30
Silvant;Olivier;67;Magglingen/Macolin;2:51:40;M30
Simperl;Burkhard;73;Winterthur;2:50:41;M30
Simpson;Guy;70;ZXrich;2:57:08;M30
Sousa;Balthasar;69;P-Portugal;2:24:44;M30
Straumann;Urs;70;St. Gallen;2:52:42;M30
Tesfaye;Eticha;74;ETH-Aethiopien;2:12:39;M30
Thallinger;Rolf;69;BXren an der Aare;2:44:31;M30
Thode;Andreas;75;Stallikon;2:37:42;M30
Tirop;Cliophus;76;KEN-Kenya;2:37:16;M30
Tschopp;Marcel;74;Chur;2:29:28;M30
Uebersax;Dan;69;Homburg;2:45:20;M30
Ukelo;Thomas;73;ZXrich;2:55:36;M30
Vermeesch;Pieter;76;ZXrich;2:29:55;M30
Vogt;Josef;74;FL-Balzers;2:44:32;M30
Vuistiner;Alexandre;75;Winterthur;2:41:44;M30
Weber;Thomas;69;D-Berlin;2:50:20;M30
Wheeler;Cecil;68;Zug;2:52:23;M30
Wieser;Marco;70;Baden;2:50:01;M30
WittensXldner;Christia;75;PfXffikon ZH;2:45:51;M30
Wurm;Harald;67;A-Wien;2:53:46;M30
Aho;Martti;64;SF-Tampere ;2:46:49;M40
BXhler;RenX;62;Rickenbach;2:52:23;M40
Baumgartner;Hans Ulric;66;GrosshXchstetten;2:57:16;M40
Baumgartner;Rolf;64;Neftenbach;2:53:21;M40
Bertuchoz;Urbain;66;Fully;2:54:32;M40
BrXndle;Beat;66;Romanshorn;2:49:33;M40
Brkic;Halid;66;A-Neustift;2:49:19;M40
Corrado;Vito;62;BrXttisellen;2:53:55;M40
Curiger;Andreas;64;Buchs ZH;2:46:02;M40
Egli;Alois;63;Winterthur;2:47:14;M40
Enz;Guido;66;Adligenswil;2:51:30;M40
Fischer;Heinz;64;Weiningen;2:55:36;M40
Fragnoli;David;64;Fribourg;2:57:12;M40
Frei;Martin;62;Zollikon;2:48:06;M40
Friedrich;Arnold;65;D-Altdorf;2:50:39;M40
Gassenschmidt;Peter;63;D-Sinzheim;2:56:22;M40
Gerber;Eric;65;Develier;2:53:49;M40
Gerstner;Dieter;63;D-Konstanz;2:49:33;M40
Goudsmit;Gerrit;65;Greifensee;2:56:53;M40
Gripp;TorbjXrn;63;Chur;2:48:25;M40
Gyagang;Gontscho;66;WXdenswil;2:50:23;M40
Hildebrandt;Wolfgang;63;D-Murg;2:52:11;M40
Honegger;Roger;64;Hombrechtikon;2:49:52;M40
Huber;Markus;63;Herrliberg;2:53:07;M40
Hubmann;Heinz;64;Frauenfeld;2:51:11;M40
JXger;Marco;64;Chur;2:46:23;M40
Kettner;Marco;66;Neuheim;2:51:42;M40
KlXsi;Daniel;64;RXti ZH;2:53:13;M40
Knobel;Josef;66;Effretikon;2:54:17;M40
Kolly;Eric;66;Rossens;2:51:59;M40
Kopecky;Joe;64;ZXrich;2:55:01;M40
Kosanke;Andreas;64;Freienbach;2:56:20;M40
MXchler;Arnold;64;Buttikon;2:47:16;M40
Maffi;Massimo;66;Breganzona;2:35:36;M40
Mattes;Martin;65;Bonstetten;2:54:33;M40
Messer;Markus;63;Flims-Dorf;2:54:27;M40
Michel;Dominique;66;Sierre;2:51:16;M40
MXller;Lothar;64;Uetikon am See;2:51:24;M40
MXller;Max W.;64;Vilters;2:47:44;M40
Oberholzer;Beat;62;DXbendorf;2:51:38;M40
Ochsner;Rolf;65;Jona;2:56:53;M40
Oggier;Pierre Michel;62;VXtroz;2:54:10;M40
Pelger;Johann;64;D-Selters;2:55:15;M40
Richard;Daniel;66;Burgdorf;2:47:14;M40
Rick;Hansueli;65;St. Gallen;2:50:24;M40
Ritter;Arno;64;A-Klaus-Weiler;2:54:00;M40
Rohn;Christoph;63;Burgdorf;2:46:17;M40
Saumweber;Bernhard;63;D-Gessertshausen;2:50:45;M40
Schenkel;Rainer;64;Herrliberg;2:50:02;M40
Scherer;Eduard;66;D-TXbingen;2:43:51;M40
Schlatter;Adrian;65;Basel;2:48:33;M40
Schmaeding;Ralf;63;D-Holzgerlingen;2:51:35;M40
Schmid;Richard;66;Paspels;2:52:38;M40
SchXnbXchler;Georg;64;ZXrich;2:47:24;M40
Sonderer;Marcel;63;Kilchberg;2:56:44;M40
Sticher;Markus;62;Waltenschwil;2:53:31;M40
Streit;Markus;63;Steffisburg;2:37:53;M40
Streule;Bruno;64;Uster;2:56:56;M40
Tschannen;Marco;66;Biberstein;2:55:51;M40
Ulrich;Daniel;65;Zug;2:44:58;M40
Vogg;Walter;64;D-Dinkelscherben;2:50:33;M40
Wolfensberger;Remo;65;RXti;2:53:01;M40
Wyser;Meinrad;63;ZXrich;2:56:16;M40
Wyss;Eric;66;St. Moritz;2:50:56;M40
Zwicky;Christian;66;Gockhausen;2:38:12;M40
Benz;Felix;60;Heerbrugg;2:55:31;M45
Castelmur;Markus;60;Bassersdorf;2:50:05;M45
Elmer;Beat;61;Hombrechtikon;2:43:56;M45
Farron;FranXois;61;DelXmont;2:53:48;M45
Fleury;Jean-Marc;61;Vicques;2:53:48;M45
Gerber;Ueli;60;Horgenberg;2:53:49;M45
Giger;Max;59;Wilen;2:56:30;M45
Gut;Viktor;60;Sempach Stadt;2:51:51;M45
Kalt;Markus;59;Wangen;2:44:58;M45
Koch;Sepp;61;Jonen AG;2:53:52;M45
Kramm;Bernd;61;D-Waldshut-Tiengen;2:53:48;M45
Leu;Urs;61;Wallisellen;2:40:31;M45
LXschner;Frank;59;D-Freudenberg;2:53:46;M45
Oertig;Karl;60;Flawil;2:57:03;M45
Pircher;Peter;60;A-Frastanz;2:56:09;M45
RXschenpXhler;Gerald;61;D-Hildrizhausen;2:53:25;M45
Sautebin;Michel;60;Courgenay;2:45:36;M45
Schenk;Manfred;61;D-Neuenstein;2:47:38;M45
Schmid;Andreas;61;Steinhausen;2:39:21;M45
SchXtz;Hansruedi;57;Udligenswil;2:53:41;M45
Vogel;Bruno;59;Langnau am Albis;2:53:04;M45
Weber;Andreas;61;FXllanden;2:53:53;M45
Widmer;Lucas;59;Uitikon;2:48:01;M45
Albers;Vincent;56;ZXrich;2:56:31;M50
Engeler;Markus;55;Langnau am Albis;2:45:14;M50
Gschwend;Peter;52;Kloten;2:37:43;M50
Troller;Rolf;52;Langnau am Albis;2:56:34;M50
Schweizer;RenX;50;Eglisau;2:50:16;M55
Scholl;Hans;46;Uetendorf;2:55:24;M60
Neuenschwander;Maja;80;Bern;2:44:23;W20
Stetsenko;Katerina;82;UKR-Ukraine;2:47:14;W20
Tikhonova;Helena;77;RUS-Moskau;2:39:52;W20
Vygovskaya;Gulnara;80;RUS-Russland;2:48:51;W20
Abramski;Nili;70;ISR-Rehovot;2:46:46;W30
Gries;Susanne;76;Port;2:53:47;W30
Jensen;Annemette L.;72;DK-Copenhagen / Denm;2:41:17;W30
Kouzmicheva;Oksana;72;RUS-Russland;2:52:16;W30
Meier;Bernadette;72;Dreien;2:47:56;W30
Vollenweider;Stephanie;76;Zofingen;2:55:41;W30
Martincova;Ivana;66;CZ-Brno;2:54:29;W40
Pinho;Natalia;64;P-Portugal;2:52:09;W40
Hoeltz;Ulrike;61;D-Karlsruhe;2:51:47;W45