package LA_0320;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        MünzenAutomat münzenAutomat = new MünzenAutomat();
        ArrayList<Integer> münzen = münzenAutomat.bekommeMünzen(39);
        for(int münze : münzen) {
            System.out.println(münze);
        }
    }
}
