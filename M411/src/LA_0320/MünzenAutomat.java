package LA_0320;

import java.util.ArrayList;

public class MünzenAutomat {
    Integer[] verfügbareMünzen = {130, 60, 10};
    int grössteMünze = 0;
    ArrayList<Integer> münzen = new ArrayList<>();

    public ArrayList<Integer> bekommeMünzen(int betrag) {
        if (betrag > 0) {
            int nächsteMünze = verfügbareMünzen[grössteMünze];
            if (nächsteMünze <= betrag) {
                münzen.add(nächsteMünze);
                betrag -= nächsteMünze;
            } else {
                grössteMünze ++;
            }
            bekommeMünzen(betrag);
        }
        return münzen;
    }

}
