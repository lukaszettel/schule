

package LA_8020;

import java.util.ArrayList;

/**
 *
 * @author Manuel Bachofner
 */
public class Player {
    private final Maze maze;
    private final ArrayList<Node> solution = new ArrayList<>();
    
    public Player(Maze maze){
        this.maze = maze;
    }

    public boolean foundWay = false;
    
    public void printSolution(ArrayList<Node> nodes){
        for (Node node : nodes) {
            if (foundWay == false) {
                if (node.isGoal()) {
                    foundWay = true;
                } else {
                    System.out.println(node.toString());
                    printSolution(node.getChildren());
                }
            }
        }
    }
}
