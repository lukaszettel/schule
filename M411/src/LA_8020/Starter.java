package LA_8020;

/**
 *
 * @author Manuel Bachofner
 */


public class Starter {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Maze maze = new Maze();
        Player player = new Player(maze);           //Erstelle den Player (die Maus)
        player.printSolution(maze.getRoot().getChildren()); //Gebe den richtigen Weg auf der Konsole aus
                
    }
    
}
