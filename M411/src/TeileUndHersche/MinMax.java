package TeileUndHersche;

public class MinMax {
    public int[] getMinAndMax(int[] ints, int min, int max) {
        int[] minAndMax = new int[2];
        if (max - min > 1) {
            int[] minMax1 = getMinAndMax(ints, min, min + (max - min) / 2);
            int[] minMax2 = getMinAndMax(ints, min + (max - min) / 2 + 1, max);
            minAndMax[0] = Math.min(minMax1[0], minMax2[0]);
            minAndMax[1] = Math.max(minMax1[1], minMax2[1]);
        } else {
            if (ints[min] < ints[max]) {
                minAndMax[0] = ints[min];
                minAndMax[1] = ints[max];
            } else {
                minAndMax[0] = ints[max];
                minAndMax[1] = ints[min];
            }
        }
        return minAndMax;
    }
}
