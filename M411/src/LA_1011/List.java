package LA_1011;

public interface List {
    public void addFirst(String element);
    public void addLast(String element);
    public String removeFirst();
    public String removeLast();
    public int size();

}
