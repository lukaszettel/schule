package LA_1011;

public class Gegenstand {
    String element;
    public Gegenstand() {

    }
    public Gegenstand(String element, Gegenstand next) {
        this.element = element;
    }

    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }
}
