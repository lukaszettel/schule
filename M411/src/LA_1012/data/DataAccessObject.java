/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LA_1012.data;

import java.util.Set;

/**
 *
 * @author Manuel Bachofner
 */
public interface DataAccessObject<T>{
    public Set<T> getTeilnehmer();
}
