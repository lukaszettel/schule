/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LA_1012.data;

import LA_1012.marathon.Teilnehmer;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author Manuel Bachofner
 *
 * DAO: Data Access Object. Kapselt den Zugriff auf Daten
 */
public class TeilnehmerDAO implements DataAccessObject<Teilnehmer> {

    private final String uri;
    private final static int NUMMMER_OF_ATTRIBUTES = 6;
    private final Set<Teilnehmer> teilnehmer = new TreeSet<>();

    public TeilnehmerDAO(String uri) {
        this.uri = uri;
        readFile();
    }

    @Override
    public Set<Teilnehmer> getTeilnehmer() {
        return teilnehmer;
    }

    private void readFile() {
        Path path = Paths.get(uri);
        try {
            List<String> lines = Files.readAllLines(path);
            System.out.println(lines.size());
            for (String line : lines) {
                teilnehmer.add(extractTeilnehmer(line));
            }
        } catch (IOException ex) {
            System.out.println(ex.getLocalizedMessage());
        }

    }

    private Teilnehmer extractTeilnehmer(String line) {
        String[] attributes = line.split(";");
        if (attributes.length == NUMMMER_OF_ATTRIBUTES) {
            Teilnehmer tn = new Teilnehmer(attributes[0], attributes[1], Integer.valueOf(attributes[2]), attributes[3]);
            tn.setZeitInSekunden(getZeitInSekunden(attributes[4]));
            tn.setKategorie(attributes[5]);
            return tn;
        } else {
            System.out.println(new IOException("Das Textfile ist nicht richtig formatiert.").getLocalizedMessage());

        }
        return null;
    }

    private int getZeitInSekunden(String formatierteZeit) {
        int zeitInSekunden = 0;
        int faktor = 60;
        String[] zeit = formatierteZeit.split(":");
        for (int i = zeit.length - 1; i >= 0; i--) {
            zeitInSekunden += i * faktor * Integer.valueOf(zeit[i]);
        }
        return zeitInSekunden;
    }

}
