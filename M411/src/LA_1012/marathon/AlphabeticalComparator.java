package LA_1012.marathon;


import java.util.Comparator;

public class AlphabeticalComparator implements Comparator<Teilnehmer> {

    @Override
    public int compare(Teilnehmer t1, Teilnehmer t2) {
        return t1.getName().compareToIgnoreCase(t2.getName());
    }

}
