/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LA_1012.marathon;

import LA_1012.data.DataAccessObject;

import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author Manuel Bachofner
 */


public class Rangliste {
    private final Set<Teilnehmer> teilnehmerListe;
    
    
    public Rangliste(DataAccessObject<Teilnehmer> dao){
        teilnehmerListe = dao.getTeilnehmer();
    }
    
    
    public void druckeRangliste(){
        Set<Teilnehmer> sortierteListe = new TreeSet<Teilnehmer>(new TimeComparator());
        sortierteListe.addAll(teilnehmerListe);
        for (Teilnehmer teilnehmer : sortierteListe) {
            System.out.println(teilnehmer.toString());
        }
    }
    
    public void druckeAlpabetischSortierteListe(){
        Set<Teilnehmer> sortierteListe = new TreeSet<Teilnehmer>(new AlphabeticalComparator());
        sortierteListe.addAll(teilnehmerListe);
        for (Teilnehmer teilnehmer : sortierteListe) {
            System.out.println(teilnehmer.toString());
        }
    }
}
