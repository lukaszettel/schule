/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LA_1012.marathon;

import LA_1012.data.TeilnehmerDAO;

/**
 *
 * @author Manuel Bachofner
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        TeilnehmerDAO dao = new TeilnehmerDAO("resources/Teilnehmerliste.txt");
        Rangliste ranking = new Rangliste(dao);
        ranking.druckeRangliste();
        ranking.druckeAlpabetischSortierteListe();
    }

}
