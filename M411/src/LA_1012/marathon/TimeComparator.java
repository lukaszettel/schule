package LA_1012.marathon;

import java.util.Comparator;

public class TimeComparator implements Comparator<Teilnehmer> {
    @Override
    public int compare(Teilnehmer t1, Teilnehmer t2) {
        return t1.getZeitInSekunden() - t2.getZeitInSekunden();
    }
}
