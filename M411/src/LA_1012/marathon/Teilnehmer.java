/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LA_1012.marathon;

/**
 *
 * @author Manuel Bachofner
 */
public class Teilnehmer implements Comparable<Teilnehmer> {
    private final String name;
    private final String vorname;
    private final int jahrgang;
    private final String land;
    
    private int zeitInSekunden;
    private String kategorie;

    public Teilnehmer(String name, String vorname, int jahrgang, String land) {
        this.name = name;
        this.vorname = vorname;
        this.jahrgang = jahrgang;
        this.land = land;
    }

    public void setZeitInSekunden(int zeitInSekunden) {
        this.zeitInSekunden = zeitInSekunden;
    }

    public void setKategorie(String kategorie) {
        this.kategorie = kategorie;
    }

    public String getName() {
        return name;
    }

    public String getVorname() {
        return vorname;
    }

    public int getJahrgang() {
        return jahrgang;
    }

    public String getLand() {
        return land;
    }

    public int getZeitInSekunden() {
        return zeitInSekunden;
    }

    public String getKategorie() {
        return kategorie;
    }

    @Override
    public String toString() {
        return "Teilnehmer{" + "name=" + name + ", vorname=" + vorname + ", jahrgang=" + jahrgang + ", land=" + land + ", zeitInSekunden=" + zeitInSekunden + ", kategorie=" + kategorie + '}';
    }


    @Override
    public int compareTo(Teilnehmer tn) {
        return zeitInSekunden - tn.zeitInSekunden;
    }
}
