package LA_0341.kara.einstieg;

import javakara.JavaKaraProgram;

/**
 * JavaKaraHelloWorld zeigt, wie eine Applikation geschrieben werden muss, die
 * Kara steuert und aus einer Entwicklungsumgebung heraus gestartet werden kann.
 *
 * @author Raimond Reichert
 * @author Andreas Blaser
 */
public class JavaKaraHelloWorld extends JavaKaraProgram {

    /**
     * JavaKara starten. Soll zu Beginn eine Welt geladen werden, muss der
     * Dateiname der Welt beim Aufruf von program.run() mitgegeben werden, also
     * zum Beispiel
     * program.run("build/classes/ch/bbbaden/m411/kara/welt/HalloWelt.world").
     */
    public static void main(String[] args) {
	JavaKaraProgram program = new JavaKaraHelloWorld();
	program.run();
    }

    /**
     * Die Methode myMainProgram() muss das auszufuehrende Hauptprogramm
     * enthalten. Wenn die Applikation nicht mit einer Welt vorinitialisiert
     * wird, muss zunaechst Kara irgendwo in der Welt plaziert werden.
     *
     * Nach der Ausfuehrung dieser Methode kann JavaKara normal weiter benutzt
     * werden.
     */
    @Override
    public void myMainProgram() {

		initWorld();

		aufgabe1();
    }

    private void aufgabe1() {
    	if (!kara.treeFront()) {
			kara.move();
			if (kara.onLeaf()) {
				kara.removeLeaf();
				aufgabe1();
				kara.putLeaf();
			} else {
				aufgabe1();
			}
			kara.move();
		} else {
    		kara.turnLeft();
    		kara.turnLeft();
		}
	}


    private void initWorld() {
	
	final int breite = 10;
	final int hoehe = 5;
	
	world.setSize(breite, hoehe);
	
	world.setTree(breite - 1, hoehe - 1, true);
	world.setLeaf(1, hoehe - 1, true);
	for (int i = 1; i < breite - 1; i += 2) {
	    world.setLeaf(i, hoehe - 1, true);
	}
	
	// Marienkaefer positionieren
	kara.setPosition(0, hoehe - 1);
	kara.setDirection(3); // Richtung: 0 = Nord, 1 = West, 2 = Sued, 3 = Ost
    }
}
