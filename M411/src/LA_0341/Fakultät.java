package LA_0341;

public class Fakultät {
    public static void main(String[] args) {
        System.out.println(fakultät(10));
    }

    private static int fakultät(int n) {
        if (n <= 1) {
            return 1;
        } else {
            return fakultät(n - 1) * n;
        }
    }
}
