package LA_1014;


import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Integer[] ints = new Integer[] {1,2,3,4,5,6};
        System.out.println(getMedian(ints));
    }

    public static double getMedian(Integer[] ints) {
        double m = 0;
        int n = ints.length;

        if (n % 2 == 1) {
            m = ints[((n + 1) / 2) - 1];
        } else {
            m = (ints[n / 2 + 1] + ints[n / 2]) / 2;
        }
        return m;
    }
}
