﻿/* -------------------------------------
* Datei:			Program.cs
* Datum:			29.11.2019
* Ersteller:		Lukas Zettel (terreActive)
* Version:		    v1.0
* Beschreibung: Ein Programm, welches die Umlaufszeit 
*                  eines Satelliten berechnet.
* --------------CHANGELOG--------------
* 
*/

using System;

namespace _7743_GlueckSpiel {
    class Program {
        public static int pPoints = 0;
        public static int cPoints = 0;
        static void Main(string[] args) {
            string again = "j";
            bool correctChoice = false;
            Random random = new Random();

            Console.Clear();
            Console.WriteLine("Willkommen zu Schere, Stein, Papier.");
            do {
                int pChoice = 0;
                int cChoice = 0;
                Console.WriteLine("1) Schere\n2) Stein\n3) Papier");
                try {
                    pChoice = Int32.Parse(Console.ReadLine());
                    cChoice = random.Next(1, 4);
                    if (pChoice >= 1 && pChoice <= 3) {
                        result(pChoice, cChoice);
                        correctChoice = true;
                        Console.WriteLine("Möchtest du weiter spielen? (j = weiter)");
                        again = Console.ReadLine();
                        Console.Clear();
                    }
                } catch (Exception e) {
                    Console.WriteLine("Es gab einen Fehler: " + e.Message);
                }
            } while (again == "j" || correctChoice == false);
        }
        public static void result(int pChoice, int cChoice) {
            Console.WriteLine("\nDu hast " + getString(pChoice) + " gewählt");
            Console.WriteLine("Der Computer hat " + getString(cChoice) + " gewählt");
            if (pChoice == cChoice) {
                Console.WriteLine("Es ist unentschieden");
            } else if ((pChoice == 1 && cChoice == 2) || (pChoice == 2 && cChoice == 3) || (pChoice == 3 && cChoice == 1)) {
                Console.WriteLine("Du hast verlohren");
               cPoints++;
            } else if ((pChoice == 2 && cChoice == 1) || (pChoice == 3 && cChoice == 2) || (pChoice == 1 && cChoice == 3)) {
                Console.WriteLine("Du hast gewonnen");
                pPoints++;
            }
            Console.WriteLine("Du hast " + pPoints + " Punkte, Der Computer hat " + cPoints + " Punkte.");
        }
        public static string getString(int choice) {
            string choiceString = "";
            if (choice == 1) {
                choiceString = "Schere";
            } else if (choice == 2) {
                choiceString = "Stein";
            } else if (choice == 3) {
                choiceString = "Papier";
            }
            return choiceString;
        }
    }
}
