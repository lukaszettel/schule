﻿using System;

namespace _1707_Email {
    class Program {
        static void Main(string[] args) {
            Console.Write("Welche Email möchten Sie zerlegen? ");
            string email = Console.ReadLine();
            string[] split = email.Split(new char[] { '.', '@' });
            for (int i = 0; i < split.Length; i++) {
                Console.Write(split[i] + " ");
            }
        }
    }
}
