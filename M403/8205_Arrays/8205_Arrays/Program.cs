﻿using System;

namespace _8205_Arrays {
    class Program {
        static void Main(string[] args) {

            /*      Auftrag 2
             * int[] numbers = new int[] { 6, 8, 7, 5, 4, 6, 17, 2, 6, 9 };
             * Console.WriteLine(numbers[6]);
             * Console.ReadLine();
             */
            /*      Auftrag 3
             * string[] names = new string[] { "Hund", "Katze", "Maus", "Elefant", "Giraffe", "Vogel" };
             * for(int i = 0; i < names.Length; i++) {
             *    Console.WriteLine(names[i]);
             * }
             */
            /*      Aufgabe 4
             * string[] names = new string[] { "Ich", "beherrsche", "Arrays", "langsam", "richtig", "gut!" };
             * for(int i = 0; i < names.Length; i++) {
             *      Console.Write(names [i] + " ");
             *  }
             *  Console.ReadLine();
             */
            /*      Aufgabe 5
             * string[] woerter = new string[10];
             * for(int i = 0; i < woerter.Length; i++) {
             *    Console.Write("Bitte geben Sie ein Wort ein: ");
             *    woerter[i] = Console.ReadLine();
             * }
             * for (int i = 0; i < woerter.Length; i++) {
             *    Console.WriteLine(woerter[i]);
             * }
             * Console.ReadLine();
             */
            /*      Aufgabe 6
             * string[] names = new string[] { "Hund", "Katze", "Maus", "Elefant", "Giraffe", "Vogel" };
             * Random r = new Random();
             * Console.WriteLine(names[r.Next(0, names.Length)]);
             * Console.ReadLine();
             */
            /*      Auftrag 7
             * string[] names = new string[] { "Hund", "Katze", "Maus", "Elefant", "Giraffe", "Vogel" };
             * Random r = new Random();
             * while (true) {
             *    Console.WriteLine(names[r.Next(0, names.Length + 1)]);
             *    Console.ReadLine();
             * }
             */
            int[] zahlen = new int[10];
            int groessteZahl = 0;
            for (int i = 0; i < zahlen.Length; i++) {
                Console.Write("Bitte geben Sie eine Zahl ein welche grösser als 0 ist: ");
                zahlen[i] = Int32.Parse(Console.ReadLine());
            }
            for(int i = 0; i < zahlen.Length; i++) {
                if(zahlen[i] > groessteZahl) {
                    groessteZahl = zahlen[i];
                }
            }
            Console.WriteLine("Die grösste Zahl ist: " + groessteZahl);
        }
    }
}
