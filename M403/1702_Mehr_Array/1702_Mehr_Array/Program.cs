﻿using System;

namespace _1702_Mehr_Array {
    class Program {
        static void Main(string[] args) {
            string alphabetString = "abcdefghijklmnopqrstuvwxyz";
            char[] alphabet = new char[26];
            for(int i = 0; i < alphabetString.Length; i++) {
                alphabet[i] = alphabetString[i];
            }
            char[] alphabetReverse = new char[26];
            for (int i = 25; i > alphabetString.Length; i--) {
                alphabet[i] = alphabetString[i];
            }
        }
    }
}
