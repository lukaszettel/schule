﻿using System;

namespace _7745_BubbleSort {
    class Program {
        static void Main(string[] args) {
            int temp;
            int[] numbers = new int[] { 1, 9, 3, 6, 5, 7 };
            for (int i = 0; i < numbers.Length; i++) {
                for (int i2 = 0; i2 < numbers.Length - 1; i2++) {
                    if (numbers[i2] > numbers[i2 + 1]) {
                        temp = numbers[i2 + 1];
                        numbers[i2 + 1] = numbers[i2];
                        numbers[i2] = temp;
                        for(int i3 = 0; i3 < numbers.Length; i3++) {
                            Console.Write(numbers[i3] + " ");
                        }
                        Console.WriteLine("\n");
                    }
                }
            }   
        }
    }
}
