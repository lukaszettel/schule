﻿using System;

namespace _7742_GlueckSpiel2 {
    class Program {
        static void Main(string[] args) {
            const int KLEINSTE_ZAHL = 1;
            const int GROESSTE_ZAHL = 100;
            bool correctAnswer = true;
            string weiter = "";
            do { 
                int versuche = 0;

                Console.Clear();
                Console.WriteLine("Denken Sie eine Zahl zwischen " + KLEINSTE_ZAHL + " und " + GROESSTE_ZAHL);
                Console.ReadLine();
                int unterereSchranke = KLEINSTE_ZAHL;
                int obersteSchranke = GROESSTE_ZAHL + 1;
                int rateZahl = unterereSchranke + (obersteSchranke - unterereSchranke) / 2;
                Console.WriteLine("Ist die Zahl " + rateZahl + "?");
                versuche++;
                do {
                    Console.WriteLine("Wahlmöglichkeiten: \ng: Gedachte Zahl ist grösser als Ratezahl \nk: Gedachte Zahl ist keiner als Ratezahl \nw: Gedachte Zahl ist gleich Ratezahl");
                    string choice = Console.ReadLine();
                    switch (choice) {
                        case "g":
                            unterereSchranke = rateZahl;
                            if (unterereSchranke == obersteSchranke) {
                                Console.WriteLine("Sie haben wohl die Zahl vergessen");
                                correctAnswer = false;
                            }
                            break;
                        case "k":
                            obersteSchranke = rateZahl;
                            if (unterereSchranke == obersteSchranke) {
                                Console.WriteLine("Sie haben wohl die Zahl vergessen");
                                correctAnswer = false;
                            }
                            break;
                        case "w":
                            Console.WriteLine("Gebrauchte Versuche: " + versuche);
                            Console.WriteLine("Möchtest du weiterspielen?");
                            weiter = Console.ReadLine();
                            break;
                        default:
                            correctAnswer = false;
                            Console.WriteLine("Nicht gültige Antwort.");
                            break;
                    }
                } while (correctAnswer == false);
            } while (weiter == "j") ;
        }
    }
}
