﻿/* -------------------------------------
* Datei:			Program.cs
* Datum:			27.11.2019
* Ersteller:		Lukas Zettel (terreActive)
* Version:		    v1.0
* Beschreibung: Ein Programm, welches die Anzahl Hasen und Hühner
*                  aus Anzahl Tiere und Anzahl Beine berechnet.
* --------------CHANGELOG--------------
* 
*/

using System;

namespace _7746_Hasen_u_Huehner {
    class Program {
        static void Main(string[] args) {
            //Variablen Definieren
            bool nochmal;
            int anzahlTiere;
            int anzahlBeine;
            int anzahlHuehner = 0;
            int anzahlHasen = 0;
            do {
                bool valideZahl = false;
                do {
                    try {
                        Console.Clear();
                        Console.Write("Wie viele Tiere haben sie gezählt? ");
                        anzahlTiere = Int32.Parse(Console.ReadLine());
                        Console.Write("Wie viele Beine haben sie gezählt? ");
                        anzahlBeine = Int32.Parse(Console.ReadLine());

                        //Anzahl Hasen und Hühner berechnen
                        anzahlHasen = (anzahlBeine - 2 * anzahlTiere) / 2;
                        anzahlHuehner = anzahlTiere - anzahlHasen;

                        //Abfragen ob es Funktioniert hat
                        if (anzahlTiere < 0 || anzahlBeine < 0) {
                            Console.WriteLine("Bitte geben Sie eine positive Zahl ein. (ENTER)");
                            Console.ReadLine();
                        } else if (anzahlBeine % 2 != 0) {
                            Console.WriteLine("Bitte geben Sie eine gerade Anzahl von Beine ein. (ENTER)");
                            Console.ReadLine();
                        } else if (anzahlHuehner < 0 || anzahlHasen < 0) {
                            Console.WriteLine("Da kann etwas nicht stimmen. Überprüfen Sie nochmal ihre Eingaben. (Anzahl Tiere: " + anzahlTiere + ", Anzahl Beine: " + anzahlBeine + ")");
                            Console.ReadLine();
                        } else {
                            valideZahl = true;
                        }
                    } catch (Exception e) {
                        Console.WriteLine("Bitte geben sie eine valide Zahl ein (" + e.Message + ") (ENTER)");
                        Console.ReadLine();
                    }
                } while (valideZahl == false);
                //Anzahl Hasen und Hühner ausgeben
                Console.WriteLine("\n" + anzahlHuehner + " Hühner. " + anzahlHasen + " Hasen.");
                
                //Fragen ob der Benutzer nochmal etwas berechnen will.
                Console.WriteLine("\nWollen Sie nochmal? (j = ja)");
                if (Console.ReadLine() == "j") {
                    nochmal = true;
                } else {
                    nochmal = false;
                }
            } while (nochmal);
        }
    }
}
