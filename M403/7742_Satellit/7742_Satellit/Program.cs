﻿/* -------------------------------------
 * Datei:			Program.cs
 * Datum:			29.11.2019
 * Ersteller:		Lukas Zettel (terreActive)
 * Version:		    v1.0
 * Beschreibung: Ein Programm, welches die Umlaufszeit 
 *                  eines Satelliten berechnet.
 * --------------CHANGELOG--------------
 * 
 */

using System;

namespace _7742_Satellit {
    class Program {
        static void Main(string[] args) {
            double flugHöhe = 0;
            bool again = true;
            do {
                try {
                    Console.Write("Flughöhe in Kilometer: ");
                    flugHöhe = Double.Parse(Console.ReadLine());
                    again = false;
                } catch (Exception e) {
                    Console.WriteLine("Bitte geben Sie eine gültige Flughöhe in Kilometer ein. Fehler: \n" + e.Message);
                }
            } while (again || flugHöhe <= 0);
            double pi = Math.PI;
            int erdRadius = 6378137;
            double erdBeschleunigung = 9.81;
            double umlaufZeit = 2 * pi / erdRadius * Math.Pow(Math.Pow(erdRadius + flugHöhe * 1000, 3) / erdBeschleunigung, 0.5);
            Console.WriteLine("\nUmlaufzeit in Stunden: " + Math.Round(umlaufZeit / 60 / 60, 3) + "h");
        }
    }
}
