import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home'
import Jobs from '../views/jobs/Jobs'
import JobDetails from '../views/jobs/JobDetails'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/jobs',
    name: 'Jobs',
    component: Jobs
  },
  {
    path: '/jobs/:id',
    name: 'JobDetails',
    component: JobDetails,
    props: true
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
