package me.lukaszettel.viergewinnt;


import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Gui gui = new Gui();
        int opponent = gui.getOpponent();

        Spieler spieler1 = new Spieler(gui.getPlayerName("1. Spieler"), 'x');

        Spieler spieler2 = (opponent == 0) ? new Spieler(gui.getPlayerName("2. Spieler"), 'o') : new Spieler("Computer", 'o');

        Board board = new Board();
        board.printBoard();

        boolean running = true;
        while (running) {
            boolean success = false;
            while (success == false) {
                int column = gui.getColumn(spieler1.getName());
                success = board.addChip(column, spieler1.getChipType());
            }
            board.printBoard();
            if (board.win(spieler1.getChipType())) {
                System.out.println(spieler1.getName() + " hat gewonnen");
                running = false;
                break;
            }
            if (opponent == 0) {
                success = false;
                while (success == false) {
                    int column = gui.getColumn(spieler2.getName());
                    success = board.addChip(column, spieler2.getChipType());
                }
            } else {
                success = false;
                while (success == false) {
                    int column = new Random().nextInt(6);
                    success = board.addChip(column, spieler2.getChipType());
                }
            }
            board.printBoard();
            if (board.win(spieler2.getChipType())) {
                System.out.println(spieler2.getName() + " hat gewonnen");
                running = false;
            }
        }
    }
}
