package me.lukaszettel.viergewinnt;

public class Board {
    private char[][] board = {
            {'-', '-', '-', '-', '-', '-', '-'},
            {'-', '-', '-', '-', '-', '-', '-'},
            {'-', '-', '-', '-', '-', '-', '-'},
            {'-', '-', '-', '-', '-', '-', '-'},
            {'-', '-', '-', '-', '-', '-', '-'},
            {'-', '-', '-', '-', '-', '-', '-'},
    };

    public void printBoard() {
        System.out.println("0 1 2 3 4 5 6");
        for (char[] row : board) {
            for (char field : row) {
                System.out.print(field + " ");
            }
            System.out.println("");
        }
        System.out.println("");
    }

    public boolean addChip(int column, char chipType) {
        boolean success = false;
        for (int i = board.length - 1; i >= 0; i--) {
            if (board[i][column] == '-') {
                board[i][column] = chipType;
                success = true;
                break;
            }
        }
        return success;
    }

    public boolean win(char charType) {
        for (int row = 0; row < board.length; row++) {
            for (int column = 0; column < board[row].length; column++) {
                try {
                    // Case horizontal
                    if (board[row][column] == charType && board[row][column + 1] == charType && board[row][column + 2] == charType && board[row][column + 3] == charType) {
                        return true;
                    // Case vertical
                    } else if (board[row][column] == charType && board[row + 1][column] == charType && board[row + 2][column] == charType && board[row + 3][column] == charType) {
                        return true;
                    // Case diagonal right
                    } else if (board[row][column] == charType && board[row + 1][column + 1] == charType && board[row + 2][column + 2] == charType && board[row + 3][column + 3] == charType) {
                        return true;
                    // Case diagonal left
                    } else if (board[row][column] == charType && board[row - 1][column - 1] == charType && board[row - 2][column - 2] == charType && board[row - 3][column - 3] == charType) {
                        return true;
                    }
                } catch (Exception e) {
                }
            }
        }
        return false;
    }
}
