package me.lukaszettel.viergewinnt;

import javax.swing.*;

public class Gui {
    public int getOpponent() {
        int option = JOptionPane.CLOSED_OPTION;
        while(option == JOptionPane.CLOSED_OPTION) {
            String[] options = new String[]{"2. Spieler", "Computer"};
            option = JOptionPane.showOptionDialog(null, "2. Spieler oder Computer?", "Wähle deinen Gegner",
                    JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,
                    null, options, options[0]);
        }
        return option;
    }

    public String getPlayerName(String title) {
        String name = null;
        while (name == null || name.equals("")) {
            name = JOptionPane.showInputDialog(null, "Wie heisst du?", title, JOptionPane.PLAIN_MESSAGE);
        }
        return name;
    }

    public int getColumn(String playerName) {
        String input = null;
        int column = 0;
        while (input == null || input.equals("")) {
            input = JOptionPane.showInputDialog(null, "Welche Spalte willst du?", playerName + " ist an der Reihe", JOptionPane.PLAIN_MESSAGE);
            column = Integer.parseInt(input);
        }
        return column;
    }
}
