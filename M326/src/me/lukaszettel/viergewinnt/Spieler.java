package me.lukaszettel.viergewinnt;

public class Spieler {
    private String name;
    private char chipType;

    public Spieler(String name, char chipType) {
        this.name = name;
        this.chipType = chipType;
    }

    public char getChipType() {
        return chipType;
    }

    public String getName() {
        return name;
    }
}
