import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Carousel from '../views/Carousel.vue'
import JQuery from '../views/JQuery.vue'
import JQueryCSS from '../views/JQueryCSS.vue'
import JQueryFrog from '../views/JQueryFrog.vue'
import Hangman from '../views/Hangman.vue'
import HexToBinary from '../views/HexToBinary.vue'
import JQuerySound from '../views/JQuerySound.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/carousel',
    name: 'Carousel',
    component: Carousel
  },
  {
    path: '/jquery',
    name: 'JQuery',
    component: JQuery
  },
  {
    path: '/jquerycss',
    name: 'JQueryCSS',
    component: JQueryCSS
  },
  {
    path: '/jqueryfrog',
    name: 'JQueryFrog',
    component: JQueryFrog
  },
  {
    path: '/hangman',
    name: 'Hangman',
    component: Hangman
  },
  {
    path: '/hextobinary',
    name: 'HexToBinary',
    component: HexToBinary
  },
  {
    path: '/jquerysound',
    name: 'JQuerySound',
    component: JQuerySound
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
