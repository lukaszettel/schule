Read-Host -AsSecureString "hasd"
function RollTheDice() {
    $diceSides = @("Gelb", "Rot", "Gr�n", "Braun", "Blau", "Geist")
    $random = Get-Random -Minimum -0 -Maximum $diceSides.Count
    return $diceSides[$random]
}

function RemoveCSV() {
    if (Test-Path $csvFilePath) {
        Remove-Item $csvFilePath
    }
}

$csvFilePath = "output.csv"

do {
    $loopInputValid = [int]::TryParse((Read-Host 'Wieviele Durchl�ufe'), [ref]$loopInput)
    if (!$loopInputValid -OR $loopInput -lt 10 -OR $loopInput -gt 1000) {
        Write-Host "Deine Eingabe ist Falsch"
    }
} while (!$loopInputValid -OR $loopInput -lt 10 -OR $loopInput -gt 1000)
$numberOfGames = $loopInput

RemoveCSV
"Anzahl Fruchtgummis;Weg L�nge;Anzahl Gewinne;Anzahl Verluste;Gewinnchance" | Out-File $csvFilePath -Append

Write-Host "Empfehlungen:"
for ($amountOfFruchtgummis = 2; $amountOfFruchtgummis -le 10; $amountOfFruchtgummis++) {
    for ($wayLenght = 6; $wayLenght -le 30; $wayLenght++) {
        $diceSides = @{}
        $diceSides['Gelb'] = $amountOfFruchtgummis
        $diceSides['Rot'] = $amountOfFruchtgummis
        $diceSides['Gr�n'] = $amountOfFruchtgummis
        $diceSides['Braun'] = $amountOfFruchtgummis
        $diceSides['Blau'] = $amountOfFruchtgummis
        $diceSides['Geist'] = $wayLenght

        $ghostWins = 0
        $playerWins = 0

        for ($i = 0; $i -lt $numberOfGames; $i++) {
            while ($true) {
                $side = RollTheDice

                $amount = $diceSides[$side]
                if ($side -eq "Geist") {
                    if ($amount -eq 1) {
                        $ghostWins++
                        break
                    }
                    else {
                        $diceSides[$side] -= 1;
                    }
                }
                else {
                    if ($amount -ne 0) {
                        $diceSides[$side] -= 1;
                    }

                    $remainingFruchtgummis = 0;
                    foreach ($diceSite in $diceSides.GetEnumerator()) {
                        if ($diceSite.Name -ne "Geist") {
                            $remainingFruchtgummis += $diceSite.Value
                        }
                    }
                    if ($remainingFruchtgummis -eq 0) {
                        $playerWins++
                        break
                    }
                }
            }
        }
        $winrate = $playerWins / ($playerWins + $ghostWins)
        if ($winrate -ge 0.6 -and $winrate -le 0.7) {
            Write-Host "Anzahl Fruchtgummis: $amountOfFruchtgummis, L�nge des Weges: $wayLenght, Winrate: $winrate(p:$playerWins + g:$ghostWins)"
        }

        "$amountOfFruchtgummis;$wayLenght;$playerWins;$ghostWins;$winrate" | Out-File $csvFilePath -Append
    }
}
$showCsv = (Read-Host -Prompt "Soll die CSV-Datei auf der Konsole angezeigt werden?[j,n]").ToLower()
if ($showCsv -eq "j") {
    $csvFile = Import-Csv -Path $csvFilePath -Delimiter ";" | sort Gewinnchance -Descending
    $csvFile | Format-Table 
}

$delCsv = (Read-Host -Prompt "Soll die CSV-Datei wieder gel�scht werden?[j,n]").ToLower()
if ($delCsv -eq "j") {
    RemoveCSV
}